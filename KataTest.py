from unittest import TestCase

from Kata import Kata

class KataTest(TestCase):
    def test_cadenaVacia(self):
        self.assertEqual(Kata().verificar("")[0], 0, "Cadena vacia")

    def test_conUnNumero(self):
        self.assertEqual(Kata().verificar("2")[0], 1, "Un numero")

    def test_conDosNumeros(self):
        self.assertEqual(Kata().verificar("3,4")[0], 2, "Dos numeros")

    def test_conNNumeros(self):
        self.assertEqual(Kata().verificar("1,2,3,4")[0], 4, "N numeros")